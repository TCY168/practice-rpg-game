package com.company;

public class Set {
    private String Role;
    private double HP;
    private double Spell;
    private double Attack;
    private double Spellatack;
    private double Defense;

    public Set(){
        this("?", 0, 0, 0, 0, 0);
    }

    public Set(String Role, double HP, double Spell, double Attack, double Spellattack, double Defense){
        this.Role = Role;
        this.HP = HP;
        this.Spell = Spell;
        this.Attack = Attack;
        this.Spellatack = Spellattack;
        this.Defense = Defense;
    }

    public String getRole(){
        return Role;
    }
    public double getHP(){
        return HP;
    }

    public double getSpell(){
        return Spell;
    }

    public double getAttack(){
        return Attack;
    }

    public double getSpellatack(){
        return Spellatack;
    }

    public double getDefense(){
        return Defense;
    }
}
